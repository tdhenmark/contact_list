@extends('system._layouts.auth')

@section('content')
<div class="content mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0">List of Contacts</h4>
          <a href="{{ route('system.login')}}" class="btn btn-primary mg-b-10 font-small">
            <i data-feather="check-square" class="mr-2"></i>Login
          </a>
        </div>
        <div class="row">
    
          <div class="col-lg-12 mt-3">
             <div class="df-example demo-table">
                <div class="table-responsive">
                  <table class="table table-hover mg-b-0">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                          <th scope="col">First Name</th>
                          <th scope="col">Last Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Contact</th>  
                        <th scope="col">Date Created</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse($contacts as  $contact)
                      <tr>
                        <th class="pt-3 pb-3">{{$contact->id}}</th>
                        <td class="pt-3 pb-3">{{Str::title($contact->first_name)}}</td>
                        <td class="pt-3 pb-3">{{Str::title($contact->last_name)}}</td>
                        <td class="pt-3 pb-3">{{Str::title($contact->email)}}</td>
                        <td class="pt-3 pb-3">{{$contact->contact}}</td>    
                        <td class="pt-3 pb-3">{{ date('M d, Y',strtotime($contact->created_at)) }}</td>                
                    
                      </tr>
                      @empty
                          <td colspan="7" class="text-center"><i>No record found yet.</i></td>
                          @endforelse
                    
                    </tbody>
                  </table>
                </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}

@stop

@section('page-scripts')

<script type="text/javascript">
     
</script>
@stop