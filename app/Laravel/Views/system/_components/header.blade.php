 <header class="navbar navbar-header navbar-header-fixed bg-black">
      <a href="#" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
      <div class="navbar-brand">
        <a href="#" class="df-logo" style="color: white !important">
          Sample
        </a>
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="#" class="df-logo" style="color: white !important">
            Sample
          </a>
          <a id="mainMenuClose" href="#"><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu d-block d-md-none ml-3" style="overflow-y: scroll;">
          <li class="nav-label mg-b-15 mt-3">Overview</li>
       

          
          <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.contacts.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Contacts</a>
          </li>
        
        </ul>
      </div><!-- navbar-menu-wrapper -->
      <div class="navbar-right">
        <div class="dropdown dropdown-profile">

          <a href="#" class="dropdown-link" data-toggle="dropdown" data-display="static">
           <div class="avatar avatar-sm"><img src="{{asset('assets/img/img1.png')}}" class="rounded-circle" alt=""></div>
          </a><!-- dropdown-link -->
          <div class="dropdown-menu dropdown-menu-right">
                       <a href="{{ route('system.logout')}}" class="dropdown-item"><i data-feather="log-out"></i>Sign Out</a>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- navbar-right -->
    </header><!-- navbar -->

