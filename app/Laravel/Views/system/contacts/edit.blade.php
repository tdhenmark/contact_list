@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
          	<div class="d-flex flex-row">
          	<i data-feather="skip-back" class="mt-1 mr-2"></i>
            <h3>Update Contact</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to update your contact.</p>
              <form method="POST" action="" enctype="multipart/form-data">
               {!!csrf_field()!!}
              <section class="mt-4">     
                  <div class="row row-sm">
                    <div class="form-group col-md-12 {{$errors->first('first_name') ? 'has-error' : NULL}}">
                      <label class="text-uppercase font-weight-bold">First Name</label>                  
                      <input type="text" class="form-control" name="first_name" value="{{old('first_name',$contacts->first_name)}}">
                      @if($errors->first('first_name'))
                        <span class="help-block">{{$errors->first('first_name')}}</span>
                      @endif
                    </div>    
                    
                    <div class="form-group col-md-12 {{$errors->first('last_name') ? 'has-error' : NULL}}">
                      <label class="text-uppercase font-weight-bold">Last Name</label>                  
                      <input type="text" class="form-control" name="last_name" value="{{old('last_name',$contacts->last_name)}}">
                      @if($errors->first('last_name'))
                        <span class="help-block">{{$errors->first('last_name')}}</span>
                      @endif
                    </div>    

                    <div class="form-group col-md-12 {{$errors->first('email') ? 'has-error' : NULL}}">
                      <label class="text-uppercase font-weight-bold">Email Address</label>                  
                      <input type="text" class="form-control" name="email" value="{{old('email',$contacts->email)}}">
                      @if($errors->first('email'))
                        <span class="help-block">{{$errors->first('email')}}</span>
                      @endif
                    </div>    

                    <div class="form-group col-md-12 {{$errors->first('contact') ? 'has-error' : NULL}}">
                      <label class="text-uppercase font-weight-bold">Contact Number</label>                  
                      <input type="text" class="form-control" name="contact" value="{{old('contact',$contacts->contact)}}">
                      @if($errors->first('contact'))
                        <span class="help-block">{{$errors->first('contact')}}</span>
                      @endif
                    </div>    
                      
                    <div class="col-md-12 text-right">
                      <button type="submit" class="btn btn-primary"><i data-feather="check-square" class="mr-2"></i>Update Contact</button>
                    </div>        
                  </div><!-- row -->
               </section>
             </form>
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop