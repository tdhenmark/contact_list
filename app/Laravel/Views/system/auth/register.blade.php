@extends('system._layouts.auth')

@section('content')
<a class="btn btn-brand-02 bg-black btn--back" href="{{ route('system.home') }}">Back to Homepage</a>

    <div class="content mt-5 content-fixed content-auth">
    <div class="container">
      <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
        <div class="media-body align-items-center d-none d-lg-flex">
          <div class="mx-wd-600">
            <img src="{{asset('assets/img/img15.png')}}" class="img-fluid" alt="">
          </div>
        </div><!-- media-body -->

        <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">

          <div class="wd-100p">
            <h3 class="tx-color-01 mg-b-5">Sign Up</h3>
            <p class="tx-color-03 tx-16 mg-b-40">Welcome back! Please sign up to continue.</p>
            @include('system._components.notifications')
            <form action="" method="POST" enctype="multipart/form-data">
                 {{ csrf_field() }}
             
              <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
                <label>Email address</label>
                <input type="text" class="form-control" name="username" value="{{old('username')}}">
                @if($errors->first('username'))
                <span class="help-block">{{$errors->first('username')}}</span>
                @endif
              </div>
            
              <div class="form-group">
                <div class="{{$errors->first('password') ? 'has-error' : NULL}}">
                    <label>New Password</label>
                    <input type="password" class="form-control" name="password" value="{{old('password')}}">
                    @if($errors->first('password'))
                    <span class="help-block">{{$errors->first('password')}}</span>
                    @endif
                </div>
            </div>
             <div class="form-group">
                <div class="{{$errors->first('password_confirmation') ? 'has-error' : NULL}}">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
                    @if($errors->first('password_confirmation'))
                    <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                    @endif
                </div>
            </div>

              <button type="submit" class="btn text-white btn-block bg-black">Sign Up</button>
               <div class="text-center mt-2">   
                  <span>Already have an account? <a href="{{route('system.login')}}" class="text-violet font-weight-bold">Sign In</a></span>
                </div>
            </form>
          </div>
        </div><!-- sign-wrapper -->
      </div><!-- media -->
    </div><!-- container -->
  </div><!-- content -->
@stop