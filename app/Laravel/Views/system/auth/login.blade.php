@extends('system._layouts.auth')

@section('content')
<a class="btn btn-brand-02 bg-black btn--back" href="{{ route('system.home') }}">Back to Homepage</a>
	<div class="content mt-5 content-fixed content-auth">
    <div class="container">
      <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
        <div class="media-body align-items-center d-none d-lg-flex">
          <div class="mx-wd-600">
            <img src="{{asset('assets/img/img15.png')}}" class="img-fluid" alt="">
          </div>
        </div><!-- media-body -->

        <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">

          <div class="wd-100p">
            <h3 class="tx-color-01 mg-b-5">Sign In</h3>
            <p class="tx-color-03 tx-16 mg-b-40">Welcome back! Please signin to continue.</p>
            @include('system._components.notifications')
            <form action="" method="POST">
              {!!csrf_field()!!}
              <div class="form-group">
                <label>Email address</label>
                <input type="text" class="form-control text-lowercase" name="username" id="username" placeholder="yourname@yourmail.com" value="{{old('username')}}">
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Password</label>
                </div>
                <input type="password" class="form-control text-lowercase" name="password" id="password" placeholder="Enter your password">
              </div>
              <button type="submit" id="login" class="bg-black btn btn-brand-02 btn-block">Sign in</button>
               <div class="text-center mt-2">   
                  <span>Don't have an account? <a href="{{route('system.register')}}" class="text-violet font-weight-bold">Sign Up</a></span>
                </div>
            </form>
          </div>
        </div><!-- sign-wrapper -->
      </div><!-- media -->
    </div><!-- container -->
  </div><!-- content -->
@stop
@section('page-scripts')


@stop