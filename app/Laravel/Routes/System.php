<?php

/*,'domain' => env("FRONTEND_URL", "wineapp.localhost.com")*/
Route::group([
		'as' => "system.",
		'namespace' => "System",
		'prefix' => "/",
		],function() {

	Route::group(['middleware'=>['system.guest']],function(){
       Route::get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		Route::post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
        Route::get('register/{redirect_uri?}',['as' => "register",'uses' => "AuthController@register"]);
        Route::post('register/{redirect_uri?}',['uses' => "AuthController@store"]);
    });

    Route::get('/',['as' => "home",'uses' => "HomeController@index"]);

	Route::group(['middleware' => ["system.auth"]], function(){
		
		Route::get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		Route::post('lock',['uses' => "AuthController@unlock"]);
		Route::get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);


		Route::group(['prefix' => "contacts",'as' => "contacts."],function(){
				Route::group(['prefix' => "/",'as' => ""],function(){
					Route::get('/',['as' => "index",'uses' => "ContactController@index"]);
					Route::get('/create',['as' => "create",'uses' => "ContactController@create"]);
					Route::post('create',['uses' => "ContactController@store"]);
					Route::get('edit/{id?}',['as' => "edit",'uses' => "ContactController@edit"]);
					Route::post('edit/{id?}',['uses' => "ContactController@update"]);
					Route::any('delete/{id?}',['as' => "destroy",'uses' => "ContactController@destroy"]);
				});
		

			});
		});
	
	});

	
	

