<?php 

namespace App\Laravel\Controllers\Frontend;

use Carbon,Auth;

class HomeController extends Controller{

	public function index(){
	
		return view('frontend.index',$this->data);
	}
}