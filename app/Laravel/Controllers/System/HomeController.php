<?php 

namespace App\Laravel\Controllers\System;

use Carbon,Auth;
use App\Laravel\Models\Contacts;
class HomeController extends Controller{

	public function index(){
	   $this->data['contacts'] = Contacts::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.index',$this->data);
	}
}