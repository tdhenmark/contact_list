<?php 

namespace App\Laravel\Controllers\System;

use App\Laravel\Models\Contacts;
use Helper, Session, Str,Carbon,Auth;
use App\Laravel\Requests\System\ContactRequest;

class ContactController extends Controller{
	protected $data;

	public function index(){
		$this->data['contacts'] = Contacts::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.contacts.index',$this->data);
	}

	public function store (ContactRequest $request) {
		try {
			$new_contacts = new Contacts;
			$user = $request->user();
            $new_contacts->fill($request->only('first_name','last_name','email','contact'));
			
			if($new_contacts->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.contacts.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function create(){
		return view('system.contacts.create',$this->data);
	}
	public function edit ($id = NULL) {
		$contacts = Contacts::find($id);
		if (!$contacts) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.contacts.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.contacts.index');	
		}

		$this->data['contacts'] = $contacts;
		return view('system.contacts.edit',$this->data);
	}
	
	public function update (ContactRequest $request, $id = NULL) {
		try {
			$contacts = Contacts::find($id);

			if (!$contacts) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.contacts.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.contacts.index');	
			}
			$user = $request->user();
        	$contacts->fill($request->only('first_name','last_name','email','contact'));
			if($contacts->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.contacts.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$contacts = Contacts::find($id);

			if (!$contacts) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.contacts.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.contacts.index');	
			}

			if($contacts->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.contacts.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}