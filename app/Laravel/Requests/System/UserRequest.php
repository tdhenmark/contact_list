<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class UserRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'username'		=> "required|unique:user,username,{$id},id,deleted_at,NULL",
			// 'email'	=> "required|unique:user,email,{$id},id,deleted_at,NULL",
			'password'	=> "required|confirmed",
		
			

		];

		if($id != 0){
			$rules['password'] = "confirmed";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			
		];
	}
}